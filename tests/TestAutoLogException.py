#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import collections
from contextlib import contextmanager
from icecream import ic
import inspect
import logging
import os
import pytest
import re
import tempfile
from typing import Dict, List, Optional, Tuple, Type, Union, cast

from logear.AutoLogException import get_fully_qualified_name, gen_logger_name_by_callable, add_log_handlers, \
    get_logger_by_callable, auto_exception_logging, adapt_autolog_exception


# Mock classes and functions to simulate the behavior
class MockCallableWithQualname:
    __module__ = 'mock.module'
    __qualname__ = 'MockCallableWithQualname'

    def __call__(self):
        pass


class MockCallableWithoutQualname:
    __module__ = 'mock.module'

    def __call__(self):
        pass


class MockNonCallable:
    __module__ = 'mock.module'


# Mocking a property
class MockPropertyClass:
    __module__ = 'mock.module'  # This should not be return value.

    def __init__(self):
        self._value = None

    @property
    def prop(self):
        return self._value

    def amethod(self):
        pass


def class_decorator(cls: type) -> type:
    def dummy_dynamic_method(*args):
        return dummy_dynamic_method.__name__
    setattr(cls, dummy_dynamic_method.__name__, dummy_dynamic_method)
    return cls


@class_decorator
class Foo:
    def dummy_method1(self):
        return "{0}'s {1}".format(Foo.__name__, self.dummy_method1.__name__)

    def dummy_method2(self):
        return "{0}'s {1}".format(Foo.__name__, self.dummy_method2.__name__)

    @staticmethod
    def dummy_static_method1(self):
        return "{0}'s {1}".format(Foo.__name__, Foo.dummy_static_method1.__name__)

    @staticmethod
    def dummy_static_method2():
        return "{0}'s {1}".format(Foo.__name__, Foo.dummy_static_method2.__name__)

    @property
    def dummy_property1(self):
        return 'dummy_property1'

    @property
    def dummy_property2(self):
        return 'dummy_property2'


class Boo(Foo):
    def dummy_method1(self):
        return "{0}'s {1}".format(Boo.__name__, self.dummy_method1.__name__)

    @staticmethod
    def dummy_static_method1():
        return "{0}'s {1}".format(Boo.__name__, Boo.dummy_static_method1.__name__)

    @property
    def dummy_property1(self):
        return 'dummy_property1'


@staticmethod
def get_log_file_from_logger(logger: logging.Logger, regex_pattern: Optional[str] = None) -> str:
    """
    Get log file path from the provided logger and checks for various assertions.

    Parameters:
    - logger (logging.Logger): The logger object whose log file needs to be read.
    - regex_pattern (str): The regular expression pattern to match against the log file name.

    Preconditions:
    - The logger must have each one instance of logging.FileHandler and logging.StreamHandler types.
    - The log file name must match the provided regex_pattern.
    - The log file must be located in the system's temporary directory.
    - The log file must exist and must be a file (not a directory).

    Returns:
    - str: log file name.

    Raises:
    - AssertionError: If any of the preconditions are not met.
    """

    handlers_dict = make_handler_dict(handlers=logger.handlers)
    assert len(handlers_dict) == 2, \
        ("Expected for logger {0} to have 2 default handlers as each instance of "
         "{1} and {2} types, but actual {3}").format(logger,
                                                     logging.StreamHandler,
                                                     logging.FileHandler,
                                                     handlers_dict)
    assert len(handlers_dict.get(logging.FileHandler)) == 1, \
        ("Expected for logger {0} to have 2 default handlers as each instance of "
         "{1} and {2} types, but actual {3}").format(logger,
                                                     logging.StreamHandler,
                                                     logging.FileHandler,
                                                     handlers_dict)
    assert len(handlers_dict.get(logging.StreamHandler)) == 1, \
        ("Expected for logger {0} to have 2 default handlers as each instance of "
         "{1} and {2} types, but actual {3}").format(logger,
                                                     logging.StreamHandler,
                                                     logging.FileHandler,
                                                     handlers_dict)

    log_file_name = cast(logging.FileHandler, handlers_dict.get(logging.FileHandler)[0]).baseFilename
    if regex_pattern:
        assert re.match(regex_pattern, os.path.basename(log_file_name))
    assert os.path.dirname(log_file_name) == tempfile.gettempdir()
    assert os.path.exists(log_file_name) and os.path.isfile(log_file_name)

    return log_file_name


def read_log_file(log_file_name: str) -> List[str]:
    """
    Reads the log file and checks for an assertion.

    Parameters:
    - log_file_name (str): The path to the log file.

    Preconditions:
    - The log file must contain at least one line.

    Returns:
    - List[str]: a list of lines read from the log file.

    Raises:
    - AssertionError: If any of the preconditions are not met.
    """

    with open(log_file_name, 'rt') as fd:
        lines = fd.readlines()
    assert len(lines) > 0, \
        "Expected to find at least one line in log file {0}, but actual not".format(log_file_name)

    return lines


def make_handler_dict(handlers: List[logging.Handler]) -> Dict[type, List[logging.Handler]]:
    result_dict = {}
    for h in handlers:
        if type(h) in list(result_dict.keys()):
            type_handlers = result_dict.get(type(h))
            cast(List, type_handlers).append(h)
        else:
            type_handlers = [h]
        result_dict.update({type(h): type_handlers})
    return result_dict


def gen_variable_name_mapping(**kwargs):
    KwargsNames = collections.namedtuple('ArgNames', ' '.join(list(kwargs.keys())))
    kwargsnames_input = {}
    for k, v in kwargs.items():
        kwargsnames_input.update({k: {k: v}})

    return KwargsNames(**kwargsnames_input)


def verify_logfile(log_file_name: str,
                   lines: List[str],
                   exception: Exception,
                   first_line_regex_pattern: str,
                   last_regex_pattern: str):
    index = 0
    # Verify 1st line in log file
    assert re.match(first_line_regex_pattern, lines[index])

    index += 1
    assert re.match("^{0}".format(re.escape('Traceback (most recent call last):')), lines[index])

    index += 1
    tokens = lines[index].split(',')
    assert len(tokens) == 3
    assert re.match('^[ \t]+File[ ]"[^"]+"', tokens[0])
    assert os.path.isfile(re.findall('[ \t]+File[ ]["]([^"]+)["]', tokens[0])[0])
    assert re.match('^[ ]line[ ][0-9]+', tokens[1])
    assert re.match('^[ ]in[ ].+', tokens[2])

    index += 2
    is_found = False
    for i in range(index, len(lines), 2):
        index = i
        if re.match("^{0}: {1}".format(type(exception).__name__, exception.args[0]), lines[i]):
            is_found = True
            break
    assert is_found, \
        "Expected to find a line of content below in {0} file, but actual not found.\n{1}".format(
            log_file_name,
            "{0}: {1}".format(
                type(exception).__name__, exception.args[0]))

    index += 1
    assert re.match("^{0}".format(re.escape('Stack (most recent call last):')), lines[index])

    index += 1
    tokens = lines[index].split(',')
    assert len(tokens) == 3
    assert re.match('^[ \t]+File[ ]"[^"]+"', tokens[0])
    assert os.path.isfile(re.findall('[ \t]+File[ ]["]([^"]+)["]', tokens[0])[0])
    assert re.match('^[ ]line[ ][0-9]+', tokens[1])
    assert re.match('^[ ]in[ ].+', tokens[2])

    # index = len(lines) - 3
    index = len(lines) - 5  # Adjustment for adding Decursion wrapper.
    assert re.match(last_regex_pattern, lines[index])


# TODO: Rewrite basic test cases with using this pattern; so that normal and exception cases can be covered by one.
@auto_exception_logging()
def throwable_function(ret_obj: Union[str, Exception]) -> str:
    if isinstance(ret_obj, str):
        return ret_obj
    else:
        raise ret_obj


class TestAutoLogException:
    test_order_mark_06052024 = 0
    @pytest.mark.run(order=test_order_mark_06052024 + 1)
    def test_get_fully_qualified_name_by_callable_with_qualname(self):
        callee = MockCallableWithQualname()
        expected = 'mock.module.MockCallableWithQualname'
        result = get_fully_qualified_name(callee)
        assert result == expected

    @pytest.mark.run(order=test_order_mark_06052024 + 2)
    def test_get_fully_qualified_name_by_callable_without_qualname(self):
        callee = MockCallableWithoutQualname()
        expected = 'mock.module.MockCallableWithoutQualname'
        result = get_fully_qualified_name(callee)
        assert result == expected

    @pytest.mark.run(order=test_order_mark_06052024 + 3)
    def test_get_fully_qualified_name_with_property(self):
        obj = MockPropertyClass()
        prop = obj.__class__.prop

        this_module = inspect.getmodule(self)
        mbrs = dict(inspect.getmembers(this_module))
        assert MockPropertyClass in mbrs.values(), \
            "Test Error: expected {0} class defined in {1} module.".format(
                MockPropertyClass.__name__, this_module.__name__)

        expected = "{0}.{1}.prop".format(this_module.__name__, MockPropertyClass.__name__)
        result = get_fully_qualified_name(prop)
        assert result == expected

    @pytest.mark.run(order=test_order_mark_06052024 + 4)
    def test_get_fully_qualified_name_by_method(self):
        obj = MockPropertyClass()

        this_module = inspect.getmodule(self)
        mbrs = dict(inspect.getmembers(this_module))
        assert MockPropertyClass in mbrs.values(), \
            "Test Error: expected {0} class defined in {1} module.".format(
                MockPropertyClass.__name__, this_module.__name__)

        expected = "{0}.{1}.{2}".format(this_module.__name__, MockPropertyClass.__name__, obj.amethod.__name__)
        result = get_fully_qualified_name(obj.amethod)
        assert result == expected

    @pytest.mark.run(order=test_order_mark_06052024 + 5)
    def test_get_fully_qualified_name_by_non_callable(self):
        callee = MockNonCallable()
        with pytest.raises(ValueError):
            get_fully_qualified_name(callee)

    @pytest.mark.run(order=test_order_mark_06052024 + 6)
    def test_get_fully_qualified_name_by_fallback_to_class_name(self):
        callee = MockCallableWithoutQualname()
        expected = 'mock.module.MockCallableWithoutQualname'
        result = get_fully_qualified_name(callee)
        assert result == expected

    test_order_mark_03282024 = test_order_mark_06052024 + 6
    @pytest.mark.run(order=test_order_mark_03282024 + 1)
    def test_get_fully_qualified_name(self):
        # About regular method
        foo_fully_name = get_fully_qualified_name(callee=Foo.dummy_method1)
        assert foo_fully_name == "{0}.{1}".format(Foo.dummy_method1.__module__, Foo.dummy_method1.__qualname__)
        foo = Foo()
        assert foo_fully_name == get_fully_qualified_name(callee=foo.dummy_method1)
        foo2 = Foo()
        assert foo_fully_name == get_fully_qualified_name(callee=foo2.dummy_method1)

        boo_fully_name = get_fully_qualified_name(callee=Boo.dummy_method1)
        assert foo_fully_name != boo_fully_name
        assert boo_fully_name == "{0}.{1}".format(Boo.dummy_method1.__module__, Boo.dummy_method1.__qualname__)
        boo = Boo()
        assert boo_fully_name == get_fully_qualified_name(callee=boo.dummy_method1)

        boo_fully_name = get_fully_qualified_name(callee=Boo.dummy_method2)
        assert boo_fully_name == "{0}.{1}".format(Boo.dummy_method2.__module__, Boo.dummy_method2.__qualname__)
        foo_fully_name = get_fully_qualified_name(callee=Foo.dummy_method2)
        assert boo_fully_name == foo_fully_name

        # About dynamic method
        foo_fully_name = get_fully_qualified_name(callee=foo.dummy_dynamic_method)
        assert foo_fully_name == "{0}.{1}".format(
            foo.dummy_dynamic_method.__module__, foo.dummy_dynamic_method.__qualname__)
        boo_fully_name = get_fully_qualified_name(callee=boo.dummy_dynamic_method)
        assert foo_fully_name == boo_fully_name

        # About static method
        foo_fully_name = get_fully_qualified_name(callee=Foo.dummy_static_method1)
        assert foo_fully_name == "{0}.{1}".format(
            Foo.dummy_static_method1.__module__, Foo.dummy_static_method1.__qualname__)
        assert foo_fully_name == get_fully_qualified_name(callee=foo.dummy_static_method1)

        boo_fully_name = get_fully_qualified_name(callee=Boo.dummy_static_method1)
        assert foo_fully_name != boo_fully_name
        assert boo_fully_name == "{0}.{1}".format(
            Boo.dummy_static_method1.__module__, Boo.dummy_static_method1.__qualname__)

        foo_fully_name = get_fully_qualified_name(callee=Foo.dummy_static_method2)
        boo_fully_name = get_fully_qualified_name(callee=Boo.dummy_static_method2)
        assert foo_fully_name == boo_fully_name

        # About property
        foo_fully_name = get_fully_qualified_name(callee=Foo.dummy_property1)
        assert foo_fully_name == "{0}.{1}".format(
            Foo.dummy_property1.fget.__module__, Foo.dummy_property1.fget.__qualname__)

        with pytest.raises(
                ValueError,
                match="Expected callable or property as value of callee argument, but actual {0}".format(
                    foo.dummy_property1)):
            get_fully_qualified_name(callee=foo.dummy_property1)

        boo_fully_name = get_fully_qualified_name(callee=Boo.dummy_property1)
        assert foo_fully_name != boo_fully_name
        assert boo_fully_name == "{0}.{1}".format(
            Boo.dummy_property1.fget.__module__, Boo.dummy_property1.fget.__qualname__)

        foo_fully_name = get_fully_qualified_name(callee=Foo.dummy_property2)
        boo_fully_name = get_fully_qualified_name(callee=Boo.dummy_property2)
        assert foo_fully_name == boo_fully_name

        function_fully_name = get_fully_qualified_name(callee=class_decorator)
        assert function_fully_name == "{0}.{1}".format(
            class_decorator.__module__, class_decorator.__qualname__)

    @pytest.mark.run(order=test_order_mark_03282024 + 2)
    def test_gen_logger_name(self):
        # About regular method
        foo = Foo()
        foo_logger_name = gen_logger_name_by_callable(callee=foo.dummy_method1)
        assert foo_logger_name == "{0}.{1}_{2}".format(foo.__module__, type(foo).__name__, hex(hash(foo)))
        assert foo_logger_name == gen_logger_name_by_callable(callee=foo.dummy_method2)
        foo2 = Foo()
        assert foo_logger_name != gen_logger_name_by_callable(callee=foo2.dummy_method1)

        boo = Boo()
        boo_logger_name = gen_logger_name_by_callable(callee=boo.dummy_method1)
        assert foo_logger_name != boo_logger_name
        assert boo_logger_name == "{0}.{1}_{2}".format(boo.__module__, type(boo).__name__, hex(hash(boo)))
        assert boo_logger_name == gen_logger_name_by_callable(callee=boo.dummy_method2)

        foo_logger_name = gen_logger_name_by_callable(callee=Foo.dummy_method1)
        assert foo_logger_name == "{0}.{1}".format(Foo.__module__, Foo.__name__)
        boo_logger_name = gen_logger_name_by_callable(callee=Boo.dummy_method1)
        assert boo_logger_name == "{0}.{1}".format(Boo.__module__, Boo.__name__)
        assert foo_logger_name != boo_logger_name

        boo_logger_name = gen_logger_name_by_callable(callee=Boo.dummy_method2)
        assert foo_logger_name == boo_logger_name

        # About dynamic method
        foo_logger_name = gen_logger_name_by_callable(callee=foo.dummy_dynamic_method)
        assert foo_logger_name == "{0}.{1}_{2}".format(foo.__module__, type(foo).__name__, hex(hash(foo)))
        boo_logger_name = gen_logger_name_by_callable(callee=boo.dummy_dynamic_method)
        assert boo_logger_name == "{0}.{1}_{2}".format(boo.__module__, type(boo).__name__, hex(hash(boo)))
        assert foo_logger_name != boo_logger_name

        # About static method
        foo_logger_name = gen_logger_name_by_callable(callee=Foo.dummy_static_method1)
        assert foo_logger_name == "{0}.{1}".format(Foo.__module__, Foo.__name__)
        assert foo_logger_name == gen_logger_name_by_callable(callee=foo.dummy_static_method1)
        assert foo_logger_name == gen_logger_name_by_callable(callee=foo2.dummy_static_method1)

        boo_logger_name = gen_logger_name_by_callable(callee=Boo.dummy_static_method1)
        assert foo_logger_name != boo_logger_name
        assert boo_logger_name == "{0}.{1}".format(Boo.__module__, Boo.__name__)

        boo_logger_name = gen_logger_name_by_callable(callee=Boo.dummy_static_method2)
        assert foo_logger_name == boo_logger_name
        boo_logger_name = gen_logger_name_by_callable(callee=boo.dummy_static_method2)
        assert foo_logger_name == boo_logger_name

        # About property
        with pytest.raises(ValueError,
                           match="Expected callable object as value of {0} argument, but actual {1}".format(
                               'callee', Foo.dummy_property1)):
            gen_logger_name_by_callable(callee=Foo.dummy_property1)

        # TODO: Need to add test for inner class (though it's covered by tests in TestAdaptAutoLogException class.)

    @pytest.mark.run(order=test_order_mark_03282024 + 3)
    def test_add_log_handlers(self):
        foo = Foo()
        logger_name = gen_logger_name_by_callable(callee=foo.dummy_method1)
        logger = logging.getLogger(name=logger_name)
        # Logger.hasHandlers is buggy and does not work.
        # assert not logger.hasHandlers(), (
        #    "Test ERROR: Expected no log handlers with {0} logger but actual {1}".format(logger, logger.handlers))
        handlers = logger.handlers
        assert len(handlers) < 1, \
            "Test ERROR: Expected no log handlers with {0} logger but actual {1}".format(logger, logger.handlers)

        add_log_handlers(logger=logger)
        # Logger.hasHandlers is buggy and not reliable.
        # assert logger.hasHandlers(), (
        #    "Expected file and stream handlers set to {0} logger but actual not".format(logger))
        assert len(logger.handlers) > 1, (
            "Expected file and stream handlers set to {0} logger but actual not".format(logger))

        handlers = make_handler_dict(handlers=logger.handlers)
        assert len(handlers.get(logging.FileHandler)) == 1, \
            "Expected a {0} configured with {1} logger.".format(logging.FileHandler, logger)
        logfile_name = cast(logging.FileHandler, handlers.get(logging.FileHandler)[0]).baseFilename
        assert os.path.isfile(logfile_name)
        assert re.search("{0}[.].+[.]log".format(re.escape(logger_name)), logfile_name)

        assert len(handlers.get(logging.StreamHandler)) == 1, \
            "Expected a {0} configured with {1} logger.".format(logging.StreamHandler, logger)

    @pytest.mark.run(order=test_order_mark_03282024 + 4)
    def test_get_logger(self):
        # logger for instance
        foo = Foo()
        foo_logger_name = gen_logger_name_by_callable(callee=foo.dummy_method1)
        foo_logger = get_logger_by_callable(callee=foo.dummy_method1)
        assert foo_logger_name == foo_logger.name
        assert foo_logger == get_logger_by_callable(callee=foo.dummy_method2)
        boo = Boo()
        boo_logger = get_logger_by_callable(callee=boo.dummy_method1)
        assert foo_logger != boo_logger
        assert boo_logger == get_logger_by_callable(callee=boo.dummy_method2)

        assert len(foo_logger.handlers) == 2
        handlers_dict = make_handler_dict(handlers=foo_logger.handlers)
        assert len(handlers_dict.get(logging.FileHandler)) == 1
        assert re.search("{0}[.].+[.]log".format(re.escape(foo_logger_name)),
                         cast(logging.FileHandler, handlers_dict.get(logging.FileHandler)[0]).baseFilename)

        assert len(handlers_dict.get(logging.StreamHandler)) == 1

        # logger for class
        foo_clz_logger_name = gen_logger_name_by_callable(callee=Foo.dummy_method1)
        foo_clz_logger = get_logger_by_callable(callee=Foo.dummy_method1)
        assert foo_clz_logger_name == foo_clz_logger.name
        assert foo_clz_logger_name != foo_logger_name
        assert foo_clz_logger == get_logger_by_callable(callee=Foo.dummy_method2)

        boo_clz_logger = get_logger_by_callable(callee=Boo.dummy_method1)
        assert foo_clz_logger != boo_clz_logger

        assert foo_clz_logger == get_logger_by_callable(callee=Boo.dummy_method2)

        # logger for static
        foo_static_logger = get_logger_by_callable(callee=foo.dummy_static_method1)
        assert foo_clz_logger == foo_static_logger
        assert foo_static_logger == get_logger_by_callable(callee=foo.dummy_static_method2)

        boo_static_logger = get_logger_by_callable(callee=boo.dummy_static_method1)
        assert boo_clz_logger == boo_static_logger

        assert foo_clz_logger == get_logger_by_callable(callee=boo.dummy_static_method2)

    def test_get_logger_fail(self):
        # case of property (not-callable)
        with pytest.raises(ValueError,
                           match=re.escape("Expected callable object as value of {0} argument, but actual {1}".format(
                               'callee', Foo.dummy_property1))):
            get_logger_by_callable(callee=Foo.dummy_property1)

    @auto_exception_logging()
    def dummy_method(self, exception_obj: Exception):
        raise exception_obj

    @pytest.mark.run(order=test_order_mark_03282024 + 5)
    def test_autolog_exception_for_bad_method(self, caplog, capsys):
        err_msg = 'Intentional exception for testing.'
        exception_obj = RuntimeError(err_msg)

        with caplog.at_level(level=logging.ERROR):
            with pytest.raises(RuntimeError, match=err_msg):
                self.dummy_method(exception_obj=exception_obj)
            captured = capsys.readouterr()

        logger = get_logger_by_callable(callee=self.dummy_method)
        assert re.match(pattern="^.+ - {0} - {1} - {2}".format(
            logger.name, 'ERROR', err_msg), string=captured.err)
        log_file_name = get_log_file_from_logger(
            logger=logger,
            regex_pattern="{0}.{1}_{2}.[0-9a-z_]+{3}log".format(
                self.__module__, type(self).__name__, hex(hash(self)), os.path.extsep))
        lines = read_log_file(log_file_name=log_file_name)

        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=exception_obj,
                       first_line_regex_pattern="^.+ - {0}.{1}_{2} - {3} - {4}".format(
                           self.__module__,
                           type(self).__name__,
                           hex(hash(self)),
                           'ERROR',
                           exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}[.]{1}[(]{2}={3}[)]".format(
                           'self',
                           self.dummy_method.__name__,
                           (list(inspect.signature(self.dummy_method).parameters.keys()))[0],
                           'exception_obj'))

    class AutoLogExceptionSake:
        dummy_method_value = 'dummy_method_value'

        @auto_exception_logging()
        def dummy_method1(self) -> str:
            return self.dummy_method_value

        default_exception_obj = RuntimeError('Default exception in parent class.')

        @auto_exception_logging()
        def bad_method1(self, exception_obj: Optional[Exception]):
            if exception_obj:
                raise exception_obj
            else:
                raise self.default_exception_obj

        @auto_exception_logging()
        def bad_method2(self, exception_obj: Optional[Exception]):
            if exception_obj:
                raise exception_obj
            else:
                raise self.default_exception_obj

        @auto_exception_logging()
        def bad_method3(self, exception_obj: Optional[Exception]):
            if exception_obj:
                raise exception_obj
            else:
                raise self.default_exception_obj

        @staticmethod
        @auto_exception_logging()
        def good_static_method1():
            pass

        @staticmethod
        @auto_exception_logging()
        def bad_static_method1(exception_obj: Optional[Exception]):
            if exception_obj:
                raise exception_obj
            else:
                raise TestAutoLogException.AutoLogExceptionSake.default_exception_obj

        @staticmethod
        @auto_exception_logging()
        def bad_static_method2(exception_obj: Optional[Exception]):
            if exception_obj:
                raise exception_obj
            else:
                raise TestAutoLogException.AutoLogExceptionSake.default_exception_obj

        @auto_exception_logging()
        @staticmethod
        def good_static_method2(param: bool):
            assert param == True

        @auto_exception_logging()   # Different order of decorator
        @staticmethod
        def bad_static_method3(exception_obj: Optional[Exception]):
            if exception_obj:
                raise exception_obj
            else:
                raise TestAutoLogException.AutoLogExceptionSake.default_exception_obj

        dummy_property_value = 'dummy_property'

        @auto_exception_logging()
        @property
        def dummy_property1(self) -> str:
            return self.dummy_property_value

        @auto_exception_logging()
        @property
        def bad_property1(self):
            raise self.default_exception_obj

        @property
        @auto_exception_logging()   # Different order of decorator
        def bad_property2(self):
            raise self.default_exception_obj

    class ChildAutoLogExceptionSake(AutoLogExceptionSake):
        default_exception_obj = RuntimeError('Default exception in child class.')

        @auto_exception_logging()
        def bad_method1(self, exception_obj: Optional[Exception]):
            if exception_obj:
                raise exception_obj
            else:
                raise self.default_exception_obj

        def call_bad_method2(self, exception_obj: Optional[Exception]):
            if exception_obj:
                super().bad_method2(exception_obj=exception_obj)
            else:
                super().bad_method2()

        @auto_exception_logging()
        def call_bad_method3(self, exception_obj: Optional[Exception]):
            if exception_obj:
                super().bad_method3(exception_obj=exception_obj)
            else:
                super().bad_method3()

        @staticmethod
        @auto_exception_logging()
        def bad_static_method1(exception_obj: Optional[Exception]):
            if exception_obj:
                raise exception_obj
            else:
                raise TestAutoLogException.AutoLogExceptionSake.default_exception_obj

        @staticmethod
        def bad_static_method2(exception_obj: Optional[Exception]):
            if exception_obj:
                raise exception_obj
            else:
                raise TestAutoLogException.AutoLogExceptionSake.default_exception_obj

        @staticmethod
        @auto_exception_logging()
        def call_bad_static_method1(exception_obj: Exception):
            TestAutoLogException.AutoLogExceptionSake.bad_static_method1(exception_obj=exception_obj)

        @auto_exception_logging()
        @property
        def bad_property1(self):
            raise TestAutoLogException.ChildAutoLogExceptionSake.default_exception_obj

    def test_autolog_exception(self):
        # About regular method
        subject = TestAutoLogException.AutoLogExceptionSake()
        logger = get_logger_by_callable(callee=subject.dummy_method1)
        assert subject.dummy_method1() == subject.dummy_method_value

        log_file_name = get_log_file_from_logger(
            logger=logger,
            regex_pattern="{0}.{1}_{2}.[0-9a-z_]+{3}log".format(
                subject.__module__, type(subject).__qualname__, hex(hash(subject)), os.path.extsep))
        with open(log_file_name, 'rt') as fd:
            lines = fd.readlines()
        assert len(lines) < 1, \
            "Expected no logs in log file {0}, but actual found line(s) below.\n{1}".format(log_file_name, lines)

    def test_autolog_exception_for_calling_parent_annotated_method_from_non_annotated_method(self):
        err_msg = 'Intentional exception for testing.'
        exception_obj = RuntimeError(err_msg)

        # Case of calling parent annotated method from non-annotated method
        child_subject = TestAutoLogException.ChildAutoLogExceptionSake()
        child_logger = get_logger_by_callable(callee=child_subject.bad_method2)
        with pytest.raises(type(exception_obj), match=err_msg):
            child_subject.call_bad_method2(exception_obj=exception_obj)

        log_file_name = get_log_file_from_logger(
            logger=child_logger,
            regex_pattern="{0}.{1}_{2}.[0-9a-z_]+{3}log".format(
                child_subject.__module__, type(child_subject).__qualname__, hex(hash(child_subject)), os.path.extsep))
        lines = read_log_file(log_file_name=log_file_name)

        assert re.match(
            "^.+ - {0}.{1}_{2} - {3} - {4}".format(child_subject.__module__,
                                                   type(child_subject).__qualname__,
                                                   hex(hash(child_subject)),
                                                   'ERROR',
                                                   exception_obj.args[0]),
            lines[0])

    def test_autolog_exception_for_calling_parent_annotated_method_from_annotated_method(self):
        err_msg = 'Intentional exception for testing.'
        exception_obj = RuntimeError(err_msg)

        child_subject = TestAutoLogException.ChildAutoLogExceptionSake()
        child_logger = get_logger_by_callable(callee=child_subject.call_bad_method3)
        with pytest.raises(type(exception_obj), match=err_msg):
            child_subject.call_bad_method3(exception_obj=exception_obj)

        log_file_name = get_log_file_from_logger(
            logger=child_logger,
            regex_pattern="{0}.{1}_{2}.[0-9a-z_]+{3}log".format(
                child_subject.__module__, type(child_subject).__qualname__, hex(hash(child_subject)), os.path.extsep))
        lines = read_log_file(log_file_name=log_file_name)

        index = 0
        assert re.match(
            "^.+ - {0}.{1}_{2} - {3} - {4}".format(child_subject.__module__,
                                                   type(child_subject).__qualname__,
                                                   hex(hash(child_subject)),
                                                   'ERROR',
                                                   exception_obj.args[0]),
            lines[index])

        index += 2
        is_found = False
        for i in range(index, len(lines), 2):
            index = i
            if re.match("^{0}: {1}".format(type(exception_obj).__name__, exception_obj.args[0]), lines[i]):
                is_found = True
                break
        assert is_found, \
            "Expected to find a line of content below in {0} file, but actual not found.\n{1}".format(
                log_file_name,
                "{0}: {1}".format(
                    type(exception_obj).__name__, exception_obj.args[0]))

        index += 1
        assert re.match("^{0}".format(re.escape('Stack (most recent call last):')), lines[index])

        # verify log for original call (calling super method) of cascade calls.
        index += 2
        is_found = False
        for i in range(index, len(lines), 2):
            index = i
            if re.match(
                    "^[ \t]+{0}".format(
                        re.escape("super().{0}({1}={2})".format(
                            TestAutoLogException.AutoLogExceptionSake.bad_method3.__name__,
                            (list(inspect.signature(TestAutoLogException.AutoLogExceptionSake.bad_method3
                                                    ).parameters.keys()))[1],
                            'exception_obj'))
                    ),
                    lines[index]):
                is_found = True
                break

        assert is_found, \
            "Expected to find a line with below phrase in log file {0}, but actual not.\nsuper().{1}({2}={3})".format(
                log_file_name,
                TestAutoLogException.AutoLogExceptionSake.bad_method3.__name__,
                (list(inspect.signature(TestAutoLogException.AutoLogExceptionSake.bad_method3
                                        ).parameters.keys()))[1],
                'exception_obj')

        # Verify log for subsequent call of cascade calls.
        is_found = False
        for i in range(index + 3, len(lines), 2):
            index = i
            if re.match(
                    "^.+ - {0}.{1}_{2} - {3} - {4}".format(child_subject.__module__,
                                                           type(child_subject).__qualname__,
                                                           hex(hash(child_subject)),
                                                           'ERROR',
                                                           exception_obj.args[0]),
                    lines[index]):
                is_found = True
                break
        assert is_found, \
            ("Expected to find log of below line indicating original call of cascade calls, but "
             "actual not found\n- {0}.{1}_{2} - {3} - {4}").format(child_subject.__module__,
                                                                   type(child_subject).__qualname__,
                                                                   hex(hash(child_subject)),
                                                                   'ERROR',
                                                                   exception_obj.args[0])

        is_found = False
        for i in range(index + 3, len(lines), 2):
            index = i
            if re.match(
                    "^[ \t]+{0}".format(
                        re.escape("child_subject.{0}({1}={2})".format(
                            child_subject.call_bad_method3.__name__,
                            (list(inspect.signature(child_subject.call_bad_method3).parameters.keys()))[0],
                            'exception_obj'))
                    ),
                    lines[index]):
                is_found = True
                break
        assert is_found, \
            "Expected to find a line with below phrase in log file {0}, but actual not.\n{1}.{2}({3}={4})".format(
                log_file_name,
                'child_subject',
                TestAutoLogException.ChildAutoLogExceptionSake.call_bad_method3.__name__,
                (list(inspect.signature(TestAutoLogException.ChildAutoLogExceptionSake.call_bad_method3
                                        ).parameters.keys()))[0],
                'exception_obj')

    def test_autolog_exception_for_good_static_method(self):
        TestAutoLogException.ChildAutoLogExceptionSake.good_static_method1()
        TestAutoLogException.ChildAutoLogExceptionSake.good_static_method2(param=True)

    def test_autolog_exception_for_good_static_method_from_instance(self, caplog):
        caplog.set_level(level=logging.DEBUG)
        ic.enable()

        instance = TestAutoLogException.ChildAutoLogExceptionSake()
        instance.good_static_method1()
        instance.good_static_method2(param=True)

    def test_autolog_exception_for_bad_static_method(self):
        err_msg = 'Intentional exception for testing.'
        exception_obj = RuntimeError(err_msg)

        # About static method
        child_logger = get_logger_by_callable(callee=TestAutoLogException.ChildAutoLogExceptionSake.bad_static_method1)
        with pytest.raises(type(exception_obj), match=err_msg):
            TestAutoLogException.ChildAutoLogExceptionSake.bad_static_method1(exception_obj=exception_obj)

        log_file_name = get_log_file_from_logger(
            logger=child_logger,
            regex_pattern="{0}.{1}.[0-9a-z_]+{2}log".format(
                TestAutoLogException.ChildAutoLogExceptionSake.__module__,
                TestAutoLogException.ChildAutoLogExceptionSake.__qualname__,
                os.path.extsep))
        lines = read_log_file(log_file_name=log_file_name)

        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=exception_obj,
                       first_line_regex_pattern="^.+ - {0} - {1} - {2}".format(
                           child_logger.name, 'ERROR', exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(
                           re.escape("{0}({1}={2})".format(
                               TestAutoLogException.ChildAutoLogExceptionSake.bad_static_method1.__qualname__,
                               (list(inspect.signature(
                                   TestAutoLogException.ChildAutoLogExceptionSake.bad_static_method1
                               ).parameters.keys()))[0],
                               'exception_obj'))))

    def test_autolog_exception_for_non_annotated_static_method(self):
        err_msg = 'Intentional exception for testing.'
        exception_obj = RuntimeError(err_msg)

        # About static method
        child_logger = get_logger_by_callable(callee=TestAutoLogException.ChildAutoLogExceptionSake.bad_static_method2)
        with pytest.raises(type(exception_obj), match=err_msg):
            TestAutoLogException.ChildAutoLogExceptionSake.bad_static_method2(exception_obj=exception_obj)

        log_file_name = get_log_file_from_logger(
            logger=child_logger,
            regex_pattern="{0}.{1}.[0-9a-z_]+{2}log".format(
                TestAutoLogException.ChildAutoLogExceptionSake.__module__,
                TestAutoLogException.ChildAutoLogExceptionSake.__qualname__,
                os.path.extsep))
        with open(log_file_name, 'rt') as fd:
            lines = fd.readlines()
        assert len(lines) < 1   # Should be no log in log file.

    def test_autolog_exception_for_calling_parent_annotated_static_method_from_annotated_static_method(self):
        err_msg = 'Intentional exception for testing.'
        exception_obj = RuntimeError(err_msg)

        child_logger = get_logger_by_callable(callee=TestAutoLogException.ChildAutoLogExceptionSake.call_bad_static_method1)
        with pytest.raises(type(exception_obj), match=err_msg):
            TestAutoLogException.ChildAutoLogExceptionSake.call_bad_static_method1(exception_obj=exception_obj)

        log_file_name = get_log_file_from_logger(logger=child_logger)
        lines = read_log_file(log_file_name=log_file_name)

        index = 0
        # Verify 1st line in log file
        assert re.match(
            "^.+ - {0} - {1} - {2}".format(child_logger.name, 'ERROR', exception_obj.args[0]),
            lines[index])

        is_found = False
        for i in range(len(lines)-1, index+3, -2):
            index = i
            if re.match(
                    "^[ \t]+{0}".format(
                        re.escape("{0}({1}={2})".format(
                            TestAutoLogException.ChildAutoLogExceptionSake.call_bad_static_method1.__qualname__,
                            (list(inspect.signature(
                                TestAutoLogException.ChildAutoLogExceptionSake.call_bad_static_method1
                            ).parameters.keys()))[0],
                            'exception_obj'))
                    ),
                    lines[index]):
                is_found = True
                break
        assert is_found, \
            "Expected to find a line with below phrase in log file {0}, but actual not.\n{1}({2}={3})".format(
                log_file_name,
                TestAutoLogException.ChildAutoLogExceptionSake.call_bad_static_method1.__qualname__,
                (list(inspect.signature(
                    TestAutoLogException.ChildAutoLogExceptionSake.call_bad_static_method1.__qualname__
                ).parameters.keys()))[0],
                'exception_obj')

        # Verify on logger file for parent class
        parent_logger = get_logger_by_callable(callee=TestAutoLogException.AutoLogExceptionSake.bad_static_method1)
        parent_log_file_name = get_log_file_from_logger(logger=parent_logger)
        lines = read_log_file(log_file_name=parent_log_file_name)

        index = 0
        # Verify 1st line in log file
        assert re.match(
            "^.+ - {0} - {1} - {2}".format(parent_logger.name, 'ERROR', exception_obj.args[0]),
            lines[index])

        is_found = False
        for i in range(len(lines)-1, index+3, -2):
            index = i
            if re.match(
                    "^[ \t]+{0}".format(
                        re.escape("{0}({1}={2})".format(
                            TestAutoLogException.AutoLogExceptionSake.bad_static_method1.__qualname__,
                            (list(inspect.signature(TestAutoLogException.AutoLogExceptionSake.bad_static_method1
                                                    ).parameters.keys()))[0],
                            'exception_obj'))
                    ),
                    lines[index]):
                is_found = True
                break
        assert is_found, \
            "Expected to find a line with below phrase in log file {0}, but actual not.\n{1}({2}={3})".format(
                log_file_name,
                TestAutoLogException.AutoLogExceptionSake.bad_static_method1.__qualname__,
                (list(inspect.signature(TestAutoLogException.AutoLogExceptionSake.bad_static_method1.__qualname__
                                        ).parameters.keys()))[0],
                'exception_obj')

    def test_autolog_exception_for_function(self, caplog, capsys):
        logger = get_logger_by_callable(callee=throwable_function)
        log_file_name = get_log_file_from_logger(
            logger=logger,
            regex_pattern="{0}.[0-9a-z_]+{1}log".format(throwable_function.__module__,
                                                                os.path.extsep))
        ret_msg = "Message by {0}".format(self.test_autolog_exception_for_function.__name__)
        assert ret_msg == throwable_function(ret_obj=ret_msg)

        with open(log_file_name, 'rt') as fd:
            lines = fd.readlines()
        assert len(lines) < 1, \
            "Expected to find no log in log file {0}, but actual found below line(s).\n{1}".format(log_file_name, lines)

        exception_obj = RuntimeError("Test exception object in {0} test method".format(
            self.test_autolog_exception_for_function.__name__))

        with caplog.at_level(level=logging.ERROR):
            with pytest.raises(type(exception_obj), match=exception_obj.args[0]):
                throwable_function(ret_obj=exception_obj)
            captured = capsys.readouterr()

        assert re.match(pattern="^.+ - {0} - {1} - {2}".format(
            logger.name, 'ERROR', exception_obj.args[0]), string=captured.err)

        lines = read_log_file(log_file_name=log_file_name)
        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=exception_obj,
                       first_line_regex_pattern="^.+ - {0} - {1} - {2}".format(
                           throwable_function.__module__, 'ERROR', exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(
                           re.escape("{0}({1}={2})".format(
                               throwable_function.__qualname__,
                               list(inspect.signature(throwable_function).parameters.keys())[0],
                               'exception_obj'))))

    # TODO: add test for function case with custom logger
    # TODO: add tests for no argument

    def test_autolog_exception_for_property(self):
        subject = TestAutoLogException.AutoLogExceptionSake()
        logger = get_logger_by_callable(callee=subject.bad_method1)

        assert subject.dummy_property1 == subject.dummy_property_value

        log_file_name = get_log_file_from_logger(logger=logger)
        with open(log_file_name, 'rt') as fd:
            lines = fd.readlines()
        assert len(lines) < 1, \
            "Expected to find no log in log file {0}, but actual found below line(s).\n{1}".format(
                log_file_name, lines)

    def test_autolog_exception_for_bad_property(self):
        subject = TestAutoLogException.AutoLogExceptionSake()
        logger = get_logger_by_callable(callee=subject.bad_method1)
        with pytest.raises(type(subject.default_exception_obj), match=subject.default_exception_obj.args[0]):
            subject.bad_property1

        log_file_name = get_log_file_from_logger(logger=logger)
        lines = read_log_file(log_file_name=log_file_name)

        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=subject.default_exception_obj,
                       first_line_regex_pattern="^.+ - {0} - {1} - {2}".format(
                           logger.name, 'ERROR', subject.default_exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(re.escape('subject.bad_property1')))

    # TODO: add tests for property's setter and deleter

    # TODO: add tests with multiple other annotations

    # TODO: add tests for annotations order
    def test_autolog_exception_for_property_wrong_annotation_order(self):
        subject = TestAutoLogException.AutoLogExceptionSake()
        logger = get_logger_by_callable(callee=subject.bad_method1)
        with pytest.raises(RecursionError,
                           match="Infinite recursion detected in {0}".format('bad_property2')):
            subject.bad_property2

        log_file_name = get_log_file_from_logger(
            logger=logger,
            regex_pattern="{0}.{1}_{2}.[0-9a-z_]+{3}log".format(
                subject.__module__, type(subject).__qualname__, hex(hash(subject)), os.path.extsep))
        with open(log_file_name, 'rt') as fd:
            lines = fd.readlines()
        assert len(lines) < 1, \
            "Expected to find no log in log file {0}, but actual not: {1}".format(log_file_name, lines)

    @staticmethod
    def get_class_for_custom_logger(
            logger1: Optional[logging.Logger],
            logger2: Optional[logging.Logger],
            default_exception: Exception = RuntimeError('Default exception by gen_sample')) -> Tuple[Type, Type]:
        default_exception_obj = default_exception

        class AutoExceptionLoggingAtCallable:
            @auto_exception_logging(logger=logger1)
            def log_to_custom_logger(self, exception_obj: Optional[Exception] = None):
                if exception_obj:
                    raise exception_obj
                else:
                    raise default_exception_obj

        class ChildAutoExceptionLoggingAtCallable(AutoExceptionLoggingAtCallable):
            @auto_exception_logging(logger=logger2)
            def log_to_custom_logger(self, exception_obj: Optional[Exception] = None):
                if exception_obj:
                    raise exception_obj
                else:
                    raise default_exception_obj

        return AutoExceptionLoggingAtCallable, ChildAutoExceptionLoggingAtCallable

    def test_autolog_exception_for_bad_method_with_custom_logger(self):
        custom_logger_name = "{0}.testing.1".format(gen_logger_name_by_callable(callee=self.test_autolog_exception))
        custom_logger = logging.Logger(name=custom_logger_name)
        add_log_handlers(logger=custom_logger)

        exception_obj = RuntimeError("Test exception object in {0}".format(hex(hash(self))))

        AutoExceptionLoggingAtCallable, _ = TestAutoLogException.get_class_for_custom_logger(
            logger1=custom_logger, logger2=None, default_exception=exception_obj)
        callable_level_sample = AutoExceptionLoggingAtCallable()

        with pytest.raises(type(exception_obj), match=exception_obj.args[0]):
            callable_level_sample.log_to_custom_logger()

        log_file_name = get_log_file_from_logger(
            logger=custom_logger,
            regex_pattern="{0}.[0-9a-z_]+{1}log".format(custom_logger_name, os.path.extsep))
        lines = read_log_file(log_file_name=log_file_name)

        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=exception_obj,
                       first_line_regex_pattern="^.+ - {0} - {1} - {2}".format(
                           custom_logger_name, 'ERROR', exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(
                           re.escape("{0}.{1}()".format('callable_level_sample',
                                                        callable_level_sample.log_to_custom_logger.__name__))),)

    def test_autolog_exception_for_override_method_with_custom_logger(self):
        custom_logger1_name = "{0}.testing.1".format(gen_logger_name_by_callable(callee=self.test_autolog_exception))
        custom_logger1 = logging.Logger(name=custom_logger1_name)
        add_log_handlers(logger=custom_logger1)

        custom_logger2_name = "{0}.testing.2".format(gen_logger_name_by_callable(callee=self.test_autolog_exception))
        custom_logger2 = logging.Logger(name=custom_logger2_name)
        add_log_handlers(logger=custom_logger2)

        exception_obj = RuntimeError("Test exception object in {0}".format(hex(hash(self))))

        _, ChildAutoExceptionLoggingAtCallable = TestAutoLogException.get_class_for_custom_logger(
            logger1=custom_logger1, logger2=custom_logger2, default_exception=exception_obj)
        child_callable_level_sample = ChildAutoExceptionLoggingAtCallable()

        with pytest.raises(type(exception_obj), match=exception_obj.args[0]):
            child_callable_level_sample.log_to_custom_logger()

        log1_file_name = get_log_file_from_logger(
            logger=custom_logger1,
            regex_pattern="{0}.[0-9a-z_]+{1}log".format(custom_logger1_name, os.path.extsep))
        with open(log1_file_name, 'rt') as fd:
            lines = fd.readlines()
        assert len(lines) < 1, \
            "Expected to find no log in log file {0}, but actual not: {1}".format(log1_file_name, lines)

        log2_file_name = get_log_file_from_logger(
            logger=custom_logger2,
            regex_pattern="{0}.[0-9a-z_]+{1}log".format(custom_logger2_name, os.path.extsep))
        lines = read_log_file(log_file_name=log2_file_name)

        verify_logfile(log_file_name=log2_file_name,
                       lines=lines,
                       exception=exception_obj,
                       first_line_regex_pattern="^.+ - {0} - {1} - {2}".format(
                           custom_logger2_name, 'ERROR', exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(
                           re.escape("{0}.{1}()".format('child_callable_level_sample',
                                                        child_callable_level_sample.log_to_custom_logger.__name__))))


class TestAdaptAutoLogException:
    @contextmanager
    def setup_logging_context(self, caplog, log_file_name: str, exception_obj: Exception):
        with open(log_file_name, 'rt') as fd:
            lines = fd.readlines()
        assert len(lines) < 1, \
            "Expected to find no log in log file {0}, but actual not: {1}".format(log_file_name, lines)

        is_ic_enabled = ic.enabled
        if is_ic_enabled:
            ic.disable()

        with caplog.at_level(level=logging.ERROR):
            with pytest.raises(type(exception_obj), match=exception_obj.args[0]):
                yield

        if is_ic_enabled:
            ic.enable()


    @pytest.mark.run(order=1)
    def test_adapt_autolog_exception_for_bad_method(self, caplog, capsys):
        exception_obj = RuntimeError("Test exception object in {0} test method".format(
            self.test_adapt_autolog_exception_for_bad_method.__name__))

        @adapt_autolog_exception()
        class AutoLogExceptionAdapted:
            def bad_method(self):
                raise exception_obj

            def regular_method(self) -> str:
                return 'regular_method'

        instance = AutoLogExceptionAdapted()
        variable_name = [k for k, v in locals().items() if v is instance][0]

        logger = get_logger_by_callable(callee=instance.bad_method)
        log_file_name = get_log_file_from_logger(
            logger=logger,
            regex_pattern="{0}.{1}_{2}.[0-9a-z_]+{3}log".format(instance.__module__,
                                                                type(instance).__qualname__,
                                                                hex(hash(instance)),
                                                                os.path.extsep))

        try:
            with self.setup_logging_context(caplog, log_file_name=log_file_name, exception_obj=exception_obj):
                instance.bad_method()
        finally:
            captured = capsys.readouterr()

        assert re.match(pattern="^.+ - {0} - {1} - {2}".format(
            logger.name, 'ERROR', exception_obj.args[0]), string=captured.err)

        lines = read_log_file(log_file_name=log_file_name)
        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=exception_obj,
                       first_line_regex_pattern="^.+ - {0}.{1}_{2} - {3} - {4}".format(instance.__module__,
                                                                                       type(instance).__qualname__,
                                                                                       hex(hash(instance)),
                                                                                       'ERROR',
                                                                                       exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(
                           re.escape("{0}.{1}()".format(variable_name, instance.bad_method.__name__))))

        assert instance.regular_method() == 'regular_method'
        assert len(lines) == len(read_log_file(log_file_name=log_file_name))

    def test_adapt_autolog_exception_for_overriding_with_decorated_one(self, caplog, capsys):
        @adapt_autolog_exception()
        class AutoLogExceptionAdapted:
            exception_obj = RuntimeError("Test exception object in {0}.{1}_{2}".format(
                self.__module__, type(self).__qualname__, hex(hash(self))))

            def bad_method1(self):
                raise self.exception_obj

        @adapt_autolog_exception()
        class ChildAutoLogExceptionAdapted(AutoLogExceptionAdapted):
            exception_obj = RuntimeError("Test exception object in {0}.{1}_{2}".format(
                self.__module__, type(self).__qualname__, hex(hash(self))))

            def bad_method1(self):
                raise self.exception_obj

        child_subject = ChildAutoLogExceptionAdapted()
        child_logger = get_logger_by_callable(callee=child_subject.bad_method1)
        with pytest.raises(type(child_subject.exception_obj), match=child_subject.exception_obj.args[0]):
            child_subject.bad_method1()

        log_file_name = get_log_file_from_logger(
            logger=child_logger,
            regex_pattern="{0}.{1}_{2}.[0-9a-z_]+{3}log".format(child_subject.__module__,
                                                                type(child_subject).__qualname__,
                                                                hex(hash(child_subject)), os.path.extsep))
        lines = read_log_file(log_file_name=log_file_name)
        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=child_subject.exception_obj,
                       first_line_regex_pattern="^.+ - {0}.{1}_{2} - {3} - {4}".format(
                           child_subject.__module__,
                           type(child_subject).__qualname__,
                           hex(hash(child_subject)),
                           'ERROR',
                           child_subject.exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(
                           re.escape("{0}.{1}()".format('child_subject', child_subject.bad_method1.__name__))))

    def test_adapt_autolog_exception_for_not_overridden_method(self, caplog, capsys):
        @adapt_autolog_exception()
        class AutoLogExceptionAdapted:
            exception_obj = RuntimeError("Test exception object in {0}.{1}_{2}".format(
                self.__module__, type(self).__qualname__, hex(hash(self))))

            def bad_method1(self):
                raise self.exception_obj

            def bad_method2(self):
                raise self.exception_obj

        @adapt_autolog_exception()
        class ChildAutoLogExceptionAdapted(AutoLogExceptionAdapted):
            exception_obj = RuntimeError("Test exception object in {0}.{1}_{2}".format(
                self.__module__, type(self).__qualname__, hex(hash(self))))

            def bad_method1(self):
                raise self.exception_obj

        child_subject = ChildAutoLogExceptionAdapted()
        child_logger = get_logger_by_callable(callee=child_subject.bad_method2)
        with pytest.raises(type(child_subject.exception_obj), match=child_subject.exception_obj.args[0]):
            child_subject.bad_method2()

        log_file_name = get_log_file_from_logger(
            logger=child_logger,
            regex_pattern="{0}.{1}_{2}.[0-9a-z_]+{3}log".format(child_subject.__module__,
                                                                type(child_subject).__qualname__,
                                                                hex(hash(child_subject)), os.path.extsep))
        lines = read_log_file(log_file_name=log_file_name)
        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=child_subject.exception_obj,
                       first_line_regex_pattern="^.+ - {0}.{1}_{2} - {3} - {4}".format(
                           child_subject.__module__,
                           type(child_subject).__qualname__,
                           hex(hash(child_subject)),
                           'ERROR',
                           child_subject.exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(
                           re.escape("{0}.{1}()".format('child_subject', child_subject.bad_method2.__name__))))

    def test_adapt_autolog_exception_for_calling_super_method(self, caplog, capsys):
        @adapt_autolog_exception()
        class AutoLogExceptionAdapted:
            exception_obj = RuntimeError("Test exception object in {0}.{1}_{2}".format(
                self.__module__, type(self).__qualname__, hex(hash(self))))

            def bad_method1(self):
                raise self.exception_obj

        class ChildAutoLogExceptionAdapted(AutoLogExceptionAdapted):
            exception_obj = RuntimeError("Test exception object in {0}.{1}_{2}".format(
                self.__module__, type(self).__qualname__, hex(hash(self))))

            def call_super_bad_method1(self):
                self.bad_method1()

            def bad_method2(self):
                raise self.exception_obj

        child_subject = ChildAutoLogExceptionAdapted()
        child_logger = get_logger_by_callable(callee=child_subject.bad_method2)
        with pytest.raises(type(child_subject.exception_obj), match=child_subject.exception_obj.args[0]):
            child_subject.bad_method2()

        log_file_name = get_log_file_from_logger(
            logger=child_logger,
            regex_pattern="{0}.{1}_{2}.[0-9a-z_]+{3}log".format(child_subject.__module__,
                                                                type(child_subject).__qualname__,
                                                                hex(hash(child_subject)), os.path.extsep))
        with open(log_file_name, 'rt') as fd:
            lines = fd.readlines()
        assert len(lines) < 1, "".format()

        with pytest.raises(type(child_subject.exception_obj), match=child_subject.exception_obj.args[0]):
            child_subject.call_super_bad_method1()

        lines = read_log_file(log_file_name=log_file_name)
        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=child_subject.exception_obj,
                       first_line_regex_pattern="^.+ - {0}.{1}_{2} - {3} - {4}".format(
                           child_subject.__module__,
                           type(child_subject).__qualname__,
                           hex(hash(child_subject)),
                           'ERROR',
                           child_subject.exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(
                           re.escape("{0}.{1}()".format('self', child_subject.bad_method1.__name__))))

    def test_adapt_autolog_exception_for_bad_static_method(self, caplog, capsys):
        exception_obj = RuntimeError("Test exception object in {0} test method".format(
            self.test_adapt_autolog_exception_for_bad_static_method.__name__))

        @adapt_autolog_exception()
        class AutoLogExceptionAdapted:
            @staticmethod
            def bad_static_method():
                raise exception_obj

            @staticmethod
            def regular_static_method():
                return "regular_static_method"

        logger = get_logger_by_callable(callee=AutoLogExceptionAdapted.bad_static_method)
        log_file_name = get_log_file_from_logger(
            logger=logger,
            regex_pattern="{0}.{1}.[0-9a-z_]+{2}log".format(AutoLogExceptionAdapted.__module__,
                                                            AutoLogExceptionAdapted.__qualname__,
                                                            os.path.extsep))

        try:
            with self.setup_logging_context(caplog, log_file_name=log_file_name, exception_obj=exception_obj):
                AutoLogExceptionAdapted.bad_static_method()
        finally:
            captured = capsys.readouterr()

        assert re.match(pattern="^.+ - {0} - {1} - {2}".format(
            logger.name, 'ERROR', exception_obj.args[0]), string=captured.err)

        lines = read_log_file(log_file_name=log_file_name)
        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=exception_obj,
                       first_line_regex_pattern="^.+ - {0}.{1} - {2} - {3}".format(AutoLogExceptionAdapted.__module__,
                                                                                   AutoLogExceptionAdapted.__qualname__,
                                                                                   'ERROR',
                                                                                   exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(
                           re.escape("{0}.{1}()".format(AutoLogExceptionAdapted.__name__,
                                                        AutoLogExceptionAdapted.bad_static_method.__name__))))

        assert AutoLogExceptionAdapted.regular_static_method() == "regular_static_method"
        assert len(lines) == len(read_log_file(log_file_name=log_file_name))

    def test_adapt_autolog_exception_for_overriding_static_bad_method(self, caplog, capsys):
        exception_obj = RuntimeError("Test exception object in {0} test method".format(
            self.test_adapt_autolog_exception_for_overriding_static_bad_method.__name__))

        @adapt_autolog_exception()
        class AutoLogExceptionAdapted:
            @staticmethod
            def bad_static_method():
                raise exception_obj

        @adapt_autolog_exception()
        class ChildAutoLogExceptionAdapted(AutoLogExceptionAdapted):
            @staticmethod
            def bad_static_method():
                raise exception_obj

        child_logger = get_logger_by_callable(callee=ChildAutoLogExceptionAdapted.bad_static_method)
        with pytest.raises(type(exception_obj), match=exception_obj.args[0]):
            ChildAutoLogExceptionAdapted.bad_static_method()

        log_file_name = get_log_file_from_logger(
            logger=child_logger,
            regex_pattern="{0}.{1}.[0-9a-z_]+{2}log".format(ChildAutoLogExceptionAdapted.__module__,
                                                            ChildAutoLogExceptionAdapted.__qualname__,
                                                            os.path.extsep))
        lines = read_log_file(log_file_name=log_file_name)
        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=exception_obj,
                       first_line_regex_pattern="^.+ - {0}.{1} - {2} - {3}".format(
                           ChildAutoLogExceptionAdapted.__module__,
                           ChildAutoLogExceptionAdapted.__qualname__,
                           'ERROR',
                           exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(
                           re.escape("{0}.{1}()".format(ChildAutoLogExceptionAdapted.__name__,
                                                        ChildAutoLogExceptionAdapted.bad_static_method.__name__))))

    def test_adapt_autolog_exception_for_bad_static_method_on_instance(self, caplog, capsys):
        exception_obj = RuntimeError("Test exception object in {0} test method".format(
            self.test_adapt_autolog_exception_for_bad_static_method_on_instance.__name__))

        @adapt_autolog_exception()
        class AutoLogExceptionAdapted:
            @staticmethod
            def bad_static_method(x, y=1):
                raise exception_obj

            @staticmethod
            def regular_static_method(x, y=1) -> Dict:
                return {'x': x, 'y': y}

        instance = AutoLogExceptionAdapted()

        logger = get_logger_by_callable(callee=AutoLogExceptionAdapted.bad_static_method)
        log_file_name = get_log_file_from_logger(
            logger=logger,
            regex_pattern="{0}.{1}.[0-9a-z_]+{2}log".format(AutoLogExceptionAdapted.__module__,
                                                                AutoLogExceptionAdapted.__qualname__,
                                                                os.path.extsep))
        try:
            with self.setup_logging_context(
                    caplog, log_file_name=log_file_name, exception_obj=exception_obj):
                instance.bad_static_method(x=1, y=2)
        finally:
            captured = capsys.readouterr()

        assert re.match(pattern="^.+ - {0} - {1} - {2}".format(
            logger.name, 'ERROR', exception_obj.args[0]), string=captured.err)

        lines = read_log_file(log_file_name=log_file_name)
        instance_name = {v:k for k,v in locals().items() if v == instance}.get(instance, None)
        assert instance_name is not None, "TEST ERROR: Failed to find instance name in locals."

        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=exception_obj,
                       first_line_regex_pattern="^.+ - {0}.{1} - {2} - {3}".format(
                           AutoLogExceptionAdapted.__module__,
                           AutoLogExceptionAdapted.__qualname__,
                           'ERROR',
                           exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(
                           re.escape("{0}.{1}".format(instance_name,
                                                        AutoLogExceptionAdapted.bad_static_method.__name__))))

        kwargs = {'x': 1, 'y': 2}
        with caplog.at_level(level=logging.ERROR):
            assert kwargs == instance.regular_static_method(*list(kwargs.values()))

        updated_liens = read_log_file(log_file_name=log_file_name)
        assert len(lines) == len(updated_liens)

    # TODO: add test for console output.
    def test_adapt_autolog_exception_for_bad_property(self, caplog, capsys):
        exception_obj = RuntimeError("Test exception object in {0} test method".format(
            self.test_adapt_autolog_exception_for_bad_property.__name__))

        @adapt_autolog_exception()
        class AutoLogExceptionAdapted:
            @property
            def bad_property(self):
                raise exception_obj

            @property
            def regular_property(self):
                return 'regular_property'

            def dummy_method(self):
                return 'dummy_method'

        subject = AutoLogExceptionAdapted()

        assert subject.regular_property == 'regular_property'

        logger = get_logger_by_callable(callee=subject.dummy_method)
        log_file_name = get_log_file_from_logger(
            logger=logger,
            regex_pattern="{0}.{1}_{2}.[0-9a-z_]+{3}log".format(subject.__module__,
                                                                type(subject).__qualname__,
                                                                hex(hash(subject)),
                                                                os.path.extsep))

        try:
            with self.setup_logging_context(caplog, log_file_name=log_file_name, exception_obj=exception_obj):
                subject.bad_property
        finally:
            captured = capsys.readouterr()

        assert re.match(pattern="^.+ - {0} - {1} - {2}".format(
            logger.name, 'ERROR', exception_obj.args[0]), string=captured.err)

        lines = read_log_file(log_file_name=log_file_name)
        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=exception_obj,
                       first_line_regex_pattern="^.+ - {0}.{1}_{2} - {3} - {4}".format(subject.__module__,
                                                                                       type(subject).__qualname__,
                                                                                       hex(hash(subject)),
                                                                                       'ERROR',
                                                                                       exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(
                           re.escape("{0}.{1}".format('subject', 'bad_property'))))

        assert subject.regular_property == 'regular_property'
        assert len(lines) == len(read_log_file(log_file_name=log_file_name))

    def test_adapt_autolog_exception_for_overriding_bad_property(self, caplog, capsys):
        @adapt_autolog_exception()
        class AutoLogExceptionAdapted:
            exception_obj = RuntimeError("Test exception object in {0}.{1}_{2}".format(
                self.__module__, type(self).__qualname__, hex(hash(self))))

            @property
            def bad_property1(self):
                raise self.exception_obj

        @adapt_autolog_exception()
        class ChildAutoLogExceptionAdapted(AutoLogExceptionAdapted):
            exception_obj = RuntimeError("Test exception object in {0}.{1}_{2}".format(
                self.__module__, type(self).__qualname__, hex(hash(self))))

            @property
            def bad_property1(self):
                raise self.exception_obj

            def dummy_method(self):
                return 'dummy_method'

        child_subject = ChildAutoLogExceptionAdapted()
        with pytest.raises(type(child_subject.exception_obj), match=child_subject.exception_obj.args[0]):
            child_subject.bad_property1

        child_logger = get_logger_by_callable(callee=child_subject.dummy_method)
        log_file_name = get_log_file_from_logger(
            logger=child_logger,
            regex_pattern="{0}.{1}_{2}.[0-9a-z_]+{3}log".format(child_subject.__module__,
                                                                type(child_subject).__qualname__,
                                                                hex(hash(child_subject)), os.path.extsep))
        lines = read_log_file(log_file_name=log_file_name)
        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=child_subject.exception_obj,
                       first_line_regex_pattern="^.+ - {0}.{1}_{2} - {3} - {4}".format(
                           child_subject.__module__,
                           type(child_subject).__qualname__,
                           hex(hash(child_subject)),
                           'ERROR',
                           child_subject.exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(
                           re.escape("{0}.{1}".format('child_subject', 'bad_property1'))))

    def test_adapt_autolog_exception_for_calling_super_bad_property(self, caplog, capsys):
        class AutoLogExceptionAdapted:
            exception_obj = RuntimeError("Test exception object in {0}.{1}_{2}".format(
                self.__module__, type(self).__qualname__, hex(hash(self))))

            @property
            def bad_property1(self):
                raise self.exception_obj

        @adapt_autolog_exception()
        class ChildAutoLogExceptionAdapted(AutoLogExceptionAdapted):
            exception_obj = RuntimeError("Test exception object in {0}.{1}_{2}".format(
                self.__module__, type(self).__qualname__, hex(hash(self))))

            def dummy_method(self):
                return 'dummy_method'

        child_subject = ChildAutoLogExceptionAdapted()
        with pytest.raises(type(child_subject.exception_obj), match=child_subject.exception_obj.args[0]):
            child_subject.bad_property1

        child_logger = get_logger_by_callable(callee=child_subject.dummy_method)
        log_file_name = get_log_file_from_logger(
            logger=child_logger,
            regex_pattern="{0}.{1}_{2}.[0-9a-z_]+{3}log".format(child_subject.__module__,
                                                                type(child_subject).__qualname__,
                                                                hex(hash(child_subject)), os.path.extsep))
        lines = read_log_file(log_file_name=log_file_name)
        verify_logfile(log_file_name=log_file_name,
                       lines=lines,
                       exception=child_subject.exception_obj,
                       first_line_regex_pattern="^.+ - {0}.{1}_{2} - {3} - {4}".format(
                           child_subject.__module__,
                           type(child_subject).__qualname__,
                           hex(hash(child_subject)),
                           'ERROR',
                           child_subject.exception_obj.args[0]),
                       last_regex_pattern="^[ \t]+{0}".format(
                           re.escape("{0}.{1}".format('child_subject', 'bad_property1'))))

    def test_grab_logger_on_instance_by_adapt_autolog_exception(self):
        @adapt_autolog_exception()
        class AutoLogExceptionAdapted:
            def throwable_function(ret_obj: Union[str, Exception]) -> str:
                if isinstance(ret_obj, str):
                    return ret_obj
                else:
                    raise ret_obj

        subject = AutoLogExceptionAdapted()
        assert callable(getattr(subject, 'grab_logger', None))
        logger = subject.grab_logger()
        assert isinstance(logger, logging.Logger)
        assert logger.name == gen_logger_name_by_callable(subject.throwable_function)
        assert logger.hasHandlers()

    def test_grab_logger_on_class_by_adapt_autolog_exception(self):
        @adapt_autolog_exception()
        class AutoLogExceptionAdapted:
            def throwable_function(ret_obj: Union[str, Exception]) -> str:
                if isinstance(ret_obj, str):
                    return ret_obj
                else:
                    raise ret_obj

        assert callable(getattr(AutoLogExceptionAdapted, 'grab_logger', None))
        logger = AutoLogExceptionAdapted.grab_logger()
        assert isinstance(logger, logging.Logger)
        assert logger.name == gen_logger_name_by_callable(AutoLogExceptionAdapted.throwable_function)
        assert logger.hasHandlers()

    # TODO: add test for custom shall_wrap callable test case.
    # TODO: add test for custom logger in decorated class test case.
    # TODO: add test for calling super method of crossing over between decorated and non-decorated classes.


def test_python_sanity_check():
    @adapt_autolog_exception()
    class AutoLogExceptionAdapted:
        def dummy_method(self):
            raise self.dummy_method.__qualname__

    subject = AutoLogExceptionAdapted()
    assert type(subject) == AutoLogExceptionAdapted

    # TODO: test inheritance


def test_class_decorator():
    foo = Foo()
    assert "{0}'s {1}".format(Foo.__name__, foo.dummy_method1.__name__) == foo.dummy_method1()


def test_type_decorated_by_class_decorator():
    foo = Foo()
    assert isinstance(foo, Foo)
    assert type(foo) == Foo


def test_is_property_callable():
    class DynamicDummy:
        @property
        def is_property_callable(self):
            return True

    assert not callable(DynamicDummy.is_property_callable)


@pytest.fixture(autouse=True)
def reset_logging():
    logger_name = "{0}.{1}".format(Foo.__module__, Boo.__qualname__)
    logging.Logger.manager.loggerDict.pop(logger_name, None)

    logger_name = "{0}.{1}".format(Foo.__module__, Foo.__qualname__)
    logging.Logger.manager.loggerDict.pop(logger_name, None)

    for clz in dict(inspect.getmembers(TestAutoLogException, predicate=inspect.isclass)).values():
        logger_name = "{0}.{1}".format(clz.__module__, clz.__qualname__)
        logging.Logger.manager.loggerDict.pop(logger_name, None)
