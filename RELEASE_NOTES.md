
1.1.4 / 2024-09-30
==================

  * Fix to handle static method call in adapt_autolog_exception function.

1.1.3 / 2024-09-24
==================

  * Fix to handle static method call on instance instead of class

0.1.2-1 / 2024-06-08
====================

  * Fixed AutoLogException.get_fully_qualified_name function for callable class object case.
