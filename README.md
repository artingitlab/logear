# logear - Python Logging Helper

## Overview

The logear is a simple/small helper library that simplifies some logging cumbersome in your Python projects. It provides a convenient way to integrate logging functionalities without much hassle.


## Features

- Auto logging an exception to a log file.
  - method/function level decorator.
    - currently support for property is experimental, and only property's getter is supported.
  - class level decorator.
    - to which method/function/property applying is customizable.
  - auto setup of log file.
    - possible to specify your custom `logging.Logger` instance instead.
  - built-in recursion detection.

## Usage

- Here is a simple example:

    ```python
    from logear.AutoLogException import auto_exception_logging, adapt_autolog_exception
    import logging
    from typing import Optional
    
    @adapt_autolog_exception
    class MyClass:
    
      @property
      def logger(self) -> logging.Logger:
        return self._logger
    
      def bad_method(self):
        # Intentionally raise NotImplementedError for sake of testing.
        raise RuntimeError('Intentional exception for sake of autologging test.')
    
    @auto_exception_logging
    def bad_function():
      raise NotImplementedError('Intentional exception for sake of autologging test.'')
    ```
    
    ```python
    myobj = MyClass()
    myobj.bad_method()
    ```
    
    Then you will find a automatic log file (i.e. /tmp/MyClass_0x7fbd30279ba0a5apy5qe.log) with a line below:
    ```
    2023-09-06 21:12:37,137 - MyClass_0x7fbd30279ba0 - ERROR - Exception caught in bad_method: NotImplementedError('Intentional exception for sake of autologging test.')
    ```
    
- Usage example of shall_wrap parameter

    - Remark: Caution at refering an inner class
    
        When you decorate your class with AutoLogException.adapt_autolog_exception annotation, you need to provide 
        custom method/function to the shall_wrap parameter to instruct skipping to wrap if an callable attribute 
        of the innner class is refered in your class.
        
        In an example below, if no function/method was given to the shall_wrap parameter, then an AttributeError 
        exception saying "'function' object has no attribute 'model_json_schema'" would have thrown at execution of 
        the statement "self.DebianRelesesSchema.model_json_schema()", because AutoLogException.adapt_autolog_exception 
        wraps an inner class (what is a callable object.)
    
    ```python
    import inspect
    from logear import AutoLogException
    from logging import Logger
    import pandas as pd
    from pydantic import BaseModel
    import requests
    from typing import Dict, List, Type
    
    ...
    
    def shall_wrap_for_debian_codename_for_kernelver(clz: Type, callee_name: str) -> bool:
        members_data = inspect.getmembers(clz)
        sub_class_names = [t[0] for t in members_data if inspect.isclass(t[1])]
    
        if callee_name in sub_class_names:
            return False
        if callee_name.startswith('_'):
            return False
        return True
    
    
    @AutoLogException.adapt_autolog_exception(shall_wrap=shall_wrap_for_debian_codename_for_kernelver)
    class DebianCodenameForKernelVersion:
        def __init__(self, firecrawl_client: FireCrawlClient, firecrawl_timeout: int = 300000):
            self.firecrawl_client = firecrawl_client
            self.firecrawl_timeout = firecrawl_timeout
    
        @property
        def debian_releases_url(self) -> str:
            return 'https://wiki.debian.org/DebianReleases'
    
        @property
        def debian_packages_url(self) -> str:
            return 'https://packages.debian.org'
    
        class DebianReleaseDataSchema(BaseModel):
            code_name: str
            release_date: str
            eol: str
            eol_lts: str
            eol_elts: str
    
        class DebianRelesesSchema(BaseModel):
            debian_releases: List['DebianCodenameForKernelVersion.DebianReleaseDataSchema']
    
        def get_payload_for_release_url(self) -> dict:
            return {
                "url": self.debian_releases_url,
                "pageOptions": {
                    "onlyMainContent": True,
                    "includeHtml": False
                },
                "extractorOptions": {
                    "mode": "llm-extraction",
                    "extractionPrompt": (
                        'From the "Production Releases" section, extract each row of Code '
                        'name, Release date, EOL (End of life date), EOL LTS, and EOL ELTS.'
                        ' Keep those data as string values by not converting data type to '
                        'anything else.'
                    ),
                    "extractionSchema": self.DebianRelesesSchema.model_json_schema()
                },
                "timeout": self.firecrawl_timeout
            }
    
        def get_debian_release_table(self) -> pd.DataFrame:
            payload = self.get_payload_for_release_url()
            ...
    ```
    
