#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from icecream import ic
import inspect
import logging
import os
import tempfile
from functools import wraps

from typing import Union, Callable, Optional, cast, Type

from decursion.Decursion import detect_recursion


def get_fully_qualified_name(callee: Union[Callable, property]) -> str:
    if not callable(callee) and not isinstance(callee, property):
        raise ValueError("Expected callable or property as value of callee argument, but actual {0}".format(callee))

    if isinstance(callee, property):
        callee = callee.fget  # Use the getter function for properties

    module = callee.__module__
    if hasattr(callee, '__qualname__'):
        qualname = callee.__qualname__
    elif hasattr(callee, '__class__'):  # For a special case that a class instance self is callable.
        qualname = callee.__class__.__name__
        module = callee.__class__.__module__
    else:
        raise AttributeError("{0} does not have both of  __qualname__ and __class__ attribute".format(callee))
    return f"{module}.{qualname}"


def gen_logger_name_by_callable(callee: Callable) -> str:
    if not callable(callee):
        raise ValueError("Expected callable object as value of {0} argument, but actual {1}".format('callee', callee))

    self_obj = getattr(callee, '__self__', None)
    if self_obj is None:
        return get_fully_qualified_name(callee=callee).rsplit(sep='.', maxsplit=1)[0]

    obj_hash = hex(hash(self_obj))
    logger_name = "{0}.{1}_{2}".format(self_obj.__module__, type(self_obj).__qualname__, obj_hash)
    return logger_name


# TODO: Allow to specify directory for log file.
def add_log_handlers(logger: logging.Logger):
    has_file_handler = False
    has_stream_handler = False
    for handler in logger.handlers:
        if not has_file_handler:
            has_file_handler = isinstance(handler, logging.FileHandler)
        if not has_stream_handler:
            has_stream_handler = isinstance(handler, logging.StreamHandler)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    if not has_file_handler:
        logfd, logfile = tempfile.mkstemp(
            prefix="{0}.".format(logger.name), suffix="{0}log".format(os.path.extsep), text=True)
        fh = logging.FileHandler(logfile)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    if not has_stream_handler:
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        logger.addHandler(ch)


def get_logger_by_callable(callee: Callable) -> logging.Logger:
    logger_name = gen_logger_name_by_callable(callee=callee)
    logger_obj = logging.getLogger(logger_name)
    add_log_handlers(logger_obj)
    return logger_obj


def auto_exception_logging(
        logger: Optional[logging.Logger] = None) -> Callable[[Union[Callable, property]], Union[Callable, property]]:
    def autolog_exception(callee: Union[Callable, property]) -> Callable:
        if type(callee) == type:
            raise ValueError("Expected either of function/method/property as {0} argument value, but actual {1}".format(
                'callee', callee))

        def gen_property_logger_name(prop: property, owner_instance: object):
            if not isinstance(prop, property):
                raise ValueError(
                    "Expected property object as value of {0} argument, but actual {1}".format('prop', prop))

            prop_mbrs = dict(filter(lambda e: isinstance(e[1], property), inspect.getmembers(type(owner_instance))))

            if not (prop in list(map(lambda p: inspect.unwrap(p.fget), prop_mbrs.values()))):
                raise ValueError(
                    "Expected {0} (value of {1} argument) held in {2} (value of {3} argument) but actual not.".format(
                        prop, 'prop', owner_instance, 'owner_instance'))

            obj_hash = hex(hash(owner_instance))
            logger_name = "{0}.{1}_{2}".format(owner_instance.__module__, type(owner_instance).__qualname__, obj_hash)
            return logger_name

        def get_logger_for_property(prop: property, owner_instance: object) -> logging.Logger:
            logger_name = gen_property_logger_name(prop=prop, owner_instance=owner_instance)
            logger_obj = logging.getLogger(logger_name)
            add_log_handlers(logger_obj)
            return logger_obj

        @detect_recursion()
        @wraps(wrapped=callee)
        def autolog_exception_wrapper(*args, **kwargs):

            if isinstance(callee, staticmethod):
                # special wrapper for static method due to necessity of removing first
                # positional argument (self object) for case of executing static method
                # on instance object rather than class object.
                underlying_func = ic(callee.__func__)
                unwrapped_func = inspect.unwrap(underlying_func)

                ic(args, kwargs)

                if len(args) > 0:
                    func_mbr = ic(getattr(args[0], underlying_func.__name__, None))
                    is_same_func = False
                    if inspect.isfunction(func_mbr) or inspect.ismethod(func_mbr):
                        is_same_func = ic(inspect.unwrap(func_mbr) == unwrapped_func)

                    if is_same_func:
                        try:
                            callargs = ic(inspect.getcallargs(underlying_func, *args, **kwargs))
                        except TypeError as e:
                            ic("TypeError in integrity check on arguments to {0} before actual call: {1}".format(
                                underlying_func, e))
                            args = ic(args[1:])  # Remove the first argument and try again
                            try:
                                callargs = ic(inspect.getcallargs(underlying_func, *args, **kwargs))
                            except TypeError as e2:
                                ic("Second TypeError in integrity check on arguments to {0} before actual call".format(
                                    underlying_func, e2))
                                raise

            try:
                if isinstance(callee, property):
                    return cast(property, callee).fget(*args)
                else:
                    return callee(*args, **kwargs)
            except Exception as e:
                if logger:
                    logger.exception(e, stack_info=True)
                else:
                    if isinstance(callee, property):
                        logger_obj = get_logger_for_property(prop=callee, owner_instance=args[0])
                    else:
                        if len(args) > 0 and getattr(args[0], callee.__name__, None) is not None:
                            logger_obj = get_logger_by_callable(callee=getattr(args[0], callee.__name__, None))
                        else:
                            logger_obj = get_logger_by_callable(callee=callee)
                    logger_obj.exception(e, stack_info=True)
                raise

        if isinstance(callee, property):
            return property(autolog_exception_wrapper)
        return autolog_exception_wrapper

    return autolog_exception


def default_shall_wrap(clz: Type, callee_name: str) -> bool:
    if callee_name.startswith('_'):
        return False
    return True


def adapt_autolog_exception(shall_wrap: Callable[[Type, str], bool] = default_shall_wrap,
                            logger: Optional[logging.Logger] = None):
    def autolog_exception_for_class(cls: type) -> type:
        mbs = dict(inspect.getmembers(cls))
        # screen to only callable and property
        callable_mbs = dict(filter(lambda t:
                                   ((callable(t[1]) and type(t[1]) != type) or isinstance(t[1], property))
                                   and not inspect.isbuiltin(t[1]),
                                   mbs.items()))

        sample_callable_name = None
        sample_callable_obj = None
        for name, callable_obj in callable_mbs.items():
            if not shall_wrap(cls, name):
                continue

            sample_callable_name = name
            sample_callable_obj = callable_obj
            autolog_exception_func = auto_exception_logging(logger=logger)

            params = list(inspect.signature(callable_obj).parameters.values()) \
                if not isinstance(callable_obj, property) else None
            if params is None or (len(params) > 0 and params[0].name in ['self', 'cls']):
                wrapped = autolog_exception_func(callee=callable_obj)
            else:   # staticmethod case
                wrapped = autolog_exception_func(callee=cls.__dict__.get(name))


            # if isinstance(callable_obj, property):
            #    setattr(cls, name, property(wrapped))
            # else:
            #    setattr(cls, name, wrapped)

            setattr(cls, name, wrapped)

        if sample_callable_name:
            def grab_logger(*args) -> logging.Logger:
                if logger:
                    return logger

                if len(args) > 0 and isinstance(args[0], cls):
                    return get_logger_by_callable(callee=getattr(args[0], sample_callable_name))
                else:
                    return get_logger_by_callable(callee=sample_callable_obj)

            if not callable_mbs.get(grab_logger.__name__):
                setattr(cls, grab_logger.__name__, grab_logger)

        return cls

    return autolog_exception_for_class
